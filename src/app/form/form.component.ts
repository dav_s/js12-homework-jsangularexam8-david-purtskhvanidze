import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Quote } from '../shared/quote.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  id = '';
  category = '';
  author = '';
  text = '';

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.http.get<Quote>('https://rest-b290b-default-rtdb.firebaseio.com/quotes/'+ this.id +'.json')
        .subscribe(result => {
          this.category = result.category;
          this.author = result.author;
          this.text = result.text;
        });
    }
  }

  addQuote() {
    const category = this.category;
    const author = this.author;
    const text = this.text;
    const body = {category, author, text};

    if (this.id != undefined) {
      this.http.put('https://rest-b290b-default-rtdb.firebaseio.com/quotes/'+ this.id +'.json', body).subscribe();
      void this.router.navigate(['/']);
    } else {
      this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
      void this.router.navigate(['/']);
    }
  }
}
