import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { QuotesComponent } from './quotes/quotes.component';


const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: '', component: QuotesComponent},
      {path: 'quotes/:id', component: QuotesComponent},
      {path: 'quotes/:id/edit', component: FormComponent},
  ]},
  {path: 'add', component: FormComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    FormComponent,
    QuotesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
