import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quote } from '../shared/quote.model';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  quotes!: Quote[] | [];
  quotesCategory = '';
  loading = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.quotesCategory = params['id'];
      if (this.quotesCategory != undefined) {
        this.getPostsByCategory();
      } else {
        this.getAllPosts();
      }
    });
  }


  getAllPosts() {
    this.loading = true;
    this.http.get<{[id: string]: Quote}>('https://rest-b290b-default-rtdb.firebaseio.com/quotes.json')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(id, quoteData.category, quoteData.author, quoteData.text);
        });
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
        this.loading = false;
      });
  }

  getPostsByCategory() {
    this.loading = true;
    this.http.get<{[id: string]: Quote}>('https://rest-b290b-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="' + this.quotesCategory + '"')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(id, quoteData.category, quoteData.author, quoteData.text);
        });
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
        this.loading = false;
      });
  }

  deleteQuote(id: string) {
    this.http.delete('https://rest-b290b-default-rtdb.firebaseio.com/quotes/'+ id +'.json').subscribe();

    const index = this.quotes.findIndex(q => q.id === id);
    this.quotes.splice(index, 1);
  }
}
